package lt.vu.mif.kursinisandroid.ui.forecast

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import lt.vu.mif.kursinisandroid.data.Country
import lt.vu.mif.kursinisandroid.data.WeatherForecast
import lt.vu.mif.kursinisandroid.helpers.OpenWeatherApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherForecastViewModel : ViewModel() {
    var weatherForecast: MutableLiveData<WeatherForecast> = MutableLiveData()

    fun loadWeatherForecast(country: Country) {
        OpenWeatherApi().getWeatherForecast(country.latlng[0], country.latlng[1])
            .enqueue(object : Callback<WeatherForecast> {
                override fun onFailure(call: Call<WeatherForecast>?, t: Throwable?) {
                    Log.e("testas", "error", t)

                }

                override fun onResponse(call: Call<WeatherForecast>, response: Response<WeatherForecast>) {
                    weatherForecast.value = response.body()!!
                    Log.d("testas", weatherForecast.value?.current?.temp.toString())
                }
            })
    }


}