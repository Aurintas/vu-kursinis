package lt.vu.mif.kursinisandroid.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import lt.vu.mif.kursinisandroid.R
import lt.vu.mif.kursinisandroid.data.Country
import lt.vu.mif.kursinisandroid.databinding.CountryListItemBinding

class CountriesAdapter(
    private val countries: List<Country>,
    private val context: Context,
    private val itemClickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    override fun getItemCount() = countries.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: CountryListItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.country_list_item,
            parent, false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(countries[position])
        holder.binding.llContainer.setOnClickListener { itemClickListener(position) }
    }
}

class ViewHolder(val binding: CountryListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(country: Country) {
        binding.apply {
            text = country.name + if (country.capital != null) ", ${country.capital}" else ""
        }
    }
}