package lt.vu.mif.kursinisandroid.helpers

import lt.vu.mif.kursinisandroid.data.WeatherForecast
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    @GET("onecall?units=metric&lang=lt")
    fun getWeatherForecast(@Query("lat") lat: Double, @Query("lon") lon: Double): Call<WeatherForecast>

    companion object {
        operator fun invoke(): OpenWeatherApi {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .client(
                    OkHttpClient.Builder()
                        .addInterceptor { chain ->
                            val url = chain
                                .request()
                                .url()
                                .newBuilder()
                                .addQueryParameter("appid", "3b6139a81ff84a6a92aea91668fbe595")
                                .build()
                            chain.proceed(chain.request().newBuilder().url(url).build())
                        }
                        .build()
                )
                .build()
                .create(OpenWeatherApi::class.java)
        }

        fun formatIconUrl(icon: String): String {
            return "https://openweathermap.org/img/wn/${icon}@2x.png"
        }
    }
}