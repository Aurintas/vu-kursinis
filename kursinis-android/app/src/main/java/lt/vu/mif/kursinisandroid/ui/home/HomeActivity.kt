package lt.vu.mif.kursinisandroid.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_home.*
import lt.vu.mif.kursinisandroid.R
import lt.vu.mif.kursinisandroid.databinding.ActivityHomeBinding
import lt.vu.mif.kursinisandroid.ui.forecast.WeatherForecastActivity

class HomeActivity : AppCompatActivity() {
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_home
        )
        binding.viewmodel = viewModel

        rvCountries.layoutManager = LinearLayoutManager(this)
        rvCountries.adapter = CountriesAdapter(viewModel.cities, this, ::handlePressCountry)
        rvCountries.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        viewModel.loadCities()
    }

    private fun handlePressCountry(position: Int) {
        val intent = Intent(this, WeatherForecastActivity::class.java)
        intent.putExtra("country", viewModel.cities[position])
        startActivity(intent)
    }
}
