package lt.vu.mif.kursinisandroid.ui.forecast

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import lt.vu.mif.kursinisandroid.R
import lt.vu.mif.kursinisandroid.data.Country
import lt.vu.mif.kursinisandroid.databinding.ActivityWeatherForecastBinding
import lt.vu.mif.kursinisandroid.helpers.OpenWeatherApi
import kotlin.math.roundToInt

class WeatherForecastActivity : AppCompatActivity() {
    private lateinit var viewModel: WeatherForecastViewModel
    private lateinit var binding: ActivityWeatherForecastBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val country = intent.getSerializableExtra("country") as Country
        title = country.capital

        viewModel = ViewModelProvider(this).get(WeatherForecastViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_weather_forecast)

        viewModel.loadWeatherForecast(country)
        viewModel.weatherForecast.observe(this, Observer { weatherForecast ->
            binding.currentTemp = weatherForecast.current.temp.roundToInt()
            binding.weatherIconUrl = OpenWeatherApi.formatIconUrl(weatherForecast.current.weather[0].icon)

            binding.rvDailyForecast.layoutManager = LinearLayoutManager(this)
            binding.rvDailyForecast.adapter = DailyForecastAdapter(weatherForecast.daily, this)
            binding.rvDailyForecast.addItemDecoration(
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
            )
        })
    }
}