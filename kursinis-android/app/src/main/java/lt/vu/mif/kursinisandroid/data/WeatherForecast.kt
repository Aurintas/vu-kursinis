package lt.vu.mif.kursinisandroid.data

data class Temp(val day: Double, val night: Double)

data class WeatherDescription(val icon: String)

data class Weather(val temp: Double, val weather: List<WeatherDescription>)

data class DailyWeather(val dt: Long, val temp: Temp, val weather: List<WeatherDescription>)

data class WeatherForecast(val current: Weather, val daily: List<DailyWeather>)