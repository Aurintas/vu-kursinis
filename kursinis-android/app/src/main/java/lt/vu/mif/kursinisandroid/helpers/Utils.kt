package lt.vu.mif.kursinisandroid.helpers

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

class Utils {
    companion object {
        @JvmStatic
        @BindingAdapter("bind:imgUrl")
        fun loadImage(imageView: ImageView, imgUrl: String?) {
            Glide.with(imageView.context).load(imgUrl).into(imageView)
        }
    }
}