package lt.vu.mif.kursinisandroid.ui.forecast

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import lt.vu.mif.kursinisandroid.R
import lt.vu.mif.kursinisandroid.data.DailyWeather
import lt.vu.mif.kursinisandroid.databinding.DailyForecastListItemBinding
import lt.vu.mif.kursinisandroid.helpers.OpenWeatherApi
import java.text.SimpleDateFormat
import java.util.*

class DailyForecastAdapter(
    private val dailyWeather: List<DailyWeather>,
    private val context: Context
) : RecyclerView.Adapter<ViewHolder>() {
    override fun getItemCount() = dailyWeather.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: DailyForecastListItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.daily_forecast_list_item,
            parent, false
        )
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dailyWeather[position])
    }
}

class ViewHolder(private val binding: DailyForecastListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(weather: DailyWeather) {
        binding.apply {
            weekDay = SimpleDateFormat("EEEE").format(Date(weather.dt * 1000))
            binding.weatherIconUrl = OpenWeatherApi.formatIconUrl(weather.weather[0].icon)
            this.weather = weather
        }
    }
}