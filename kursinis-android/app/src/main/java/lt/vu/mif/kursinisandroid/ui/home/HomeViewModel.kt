package lt.vu.mif.kursinisandroid.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import lt.vu.mif.kursinisandroid.data.Country
import java.io.IOException
import java.util.*

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    val cities = ArrayList<Country>()

    fun loadCities() {
        val gson = Gson()
        val listCityType = object : TypeToken<List<Country>>() {}.type

        try {
            val jsonString = context.assets.open("countries.json").bufferedReader().use { it.readText() }
            val loadedCountries: List<Country> = gson.fromJson(jsonString, listCityType)
            cities.addAll(loadedCountries)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        }
    }
}
