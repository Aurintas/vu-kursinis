package lt.vu.mif.kursinisandroid.data

import java.io.Serializable

data class Country(
    val capital: String?,
    val name: String,
    val latlng: List<Double>
) : Serializable