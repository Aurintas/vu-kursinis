//
//  OpenWeatherApi.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-10.
//  Copyright © 2020 VU. All rights reserved.
//

import Foundation

struct OpenWeatherApi {
    static var API_KEY = "3b6139a81ff84a6a92aea91668fbe595"
    
    static func getWeatherForecast(for city: City, completionHandler: @escaping (WeatherForecast) -> Void) {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(city.lat)&lon=\(city.lon)&units=metric&lang=lt&appid=\(API_KEY)")!
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with fetching films: \(error)")
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }
            
            if let data = data {
                let forecast = try! JSONDecoder().decode(WeatherForecast.self, from: data)
                completionHandler(forecast)
            }
        })
        task.resume()
    }
}
