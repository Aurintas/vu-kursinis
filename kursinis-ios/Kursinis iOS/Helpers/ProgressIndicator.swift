//
//  ProgressIndicator.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-10.
//  Copyright © 2020 VU. All rights reserved.
//

import Foundation
import SwiftUI

struct ProgressIndicator: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        let view = UIActivityIndicatorView()
        view.startAnimating()
        return view
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
    }
}
