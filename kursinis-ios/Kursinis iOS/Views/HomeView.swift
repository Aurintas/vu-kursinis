//
//  Home.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-07.
//  Copyright © 2020 VU. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var viewModel: HomeViewModel
    
    var body: some View {
        NavigationView {
            List(viewModel.cities) { city in
                NavigationLink(destination: WeatherForecastView(viewModel: WeatherForecastViewModel(city: city))) {
                    Text("\(city.country), \(city.name)")
                }.padding(.trailing, -32.0).padding(.vertical, 11)
            }
            .onAppear {
                self.viewModel.loadCities()
            }
            .navigationBarTitle("Orai Sostinėse", displayMode: .inline)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeViewModel())
    }
}
