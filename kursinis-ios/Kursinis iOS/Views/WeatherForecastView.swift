//
//  WeatherForecastView.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-07.
//  Copyright © 2020 VU. All rights reserved.
//

import SwiftUI

struct WeatherForecastView: View {
    @ObservedObject var viewModel: WeatherForecastViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            if (viewModel.weatherForecast != nil) {
                weatherForecast
                Spacer()
            } else {
                loading
            }
        }
        .onAppear {
            self.viewModel.fetchCurrentWeather()
        }
        .navigationBarTitle(Text(viewModel.city.name), displayMode: .inline)
    }
    
    var weatherForecast: some View {
        let weatherForecast = viewModel.weatherForecast!
        let current = weatherForecast.current
        let url = URL(string: "http://openweathermap.org/img/wn/\(current.weather[0].icon)@2x.png")!
        
        return Section {
            URLImage(url: url, placeholder: ProgressIndicator()).frame(width: 100, height: 100)
            
            HStack() {
                Text(String(format: "%1.0f", current.temp)).font(.system(size: 32)).fontWeight(.heavy)
                Text("°C").font(.system(size: 20)).fontWeight(.heavy).padding(.top, -10).padding(.leading, -4)
            }.padding(.bottom, 16)
            
            ForEach(weatherForecast.daily) { day in
                self.dayForecast(day: day)
            }
        }
    }
    
    func dayForecast(day: DailyWeather) -> some View {
        let date = Date(timeIntervalSince1970: day.dt)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let url = URL(string: "http://openweathermap.org/img/wn/\(day.weather[0].icon)@2x.png")!
        
        return Section {
            HStack(spacing: 0) {
                Text(dateFormatter.string(from: date)).font(.system(size: 16))
                Spacer()
                Text(String(format: "%1.0f", day.temp.day)).font(.system(size: 16)).fontWeight(.heavy)
                Text(String(format: " / %1.0f", day.temp.night)).font(.system(size: 16))
                URLImage(url: url, placeholder: ProgressIndicator()).frame(width: 20, height: 20).padding(.leading, 16)
            }.padding(.horizontal, 16)
            
            Divider()
        }
    }
    
    var loading: some View {
        ProgressIndicator()
    }
}

struct WeatherForecastView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = WeatherForecastViewModel(city: City(country: "Lithuania", name: "Vilnius", lon: 24, lat: 56))
        return WeatherForecastView(viewModel: viewModel)
    }
}
