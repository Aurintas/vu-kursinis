//
//  HomeViewModel.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-10.
//  Copyright © 2020 VU. All rights reserved.
//

import Foundation

class HomeViewModel: ObservableObject {
    @Published var cities: [City] = []
    
    func loadCities() {
        if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
            let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            self.cities = try! JSONDecoder().decode([City].self, from: data)
        }
    }
}
