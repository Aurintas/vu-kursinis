//
//  WeatherForecastViewModel.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-10.
//  Copyright © 2020 VU. All rights reserved.
//

import Foundation

class WeatherForecastViewModel: ObservableObject {
    @Published var city: City
    @Published var weatherForecast: WeatherForecast?
    
    init(city: City) {
        self.city = city
    }
    
    func fetchCurrentWeather() {
        OpenWeatherApi.getWeatherForecast(for: city) { (weatherForecast) in
            DispatchQueue.main.async {
                self.weatherForecast = weatherForecast
            }
        }
    }
}
