//
//  WeatherForecast.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-10.
//  Copyright © 2020 VU. All rights reserved.
//

struct Temp: Decodable {
    let day: Double
    let night: Double
}

struct WeatherDescription: Decodable {
    let icon: String
}

struct Weather: Decodable {
    let temp: Double
    let weather: [WeatherDescription]
}

struct DailyWeather: Decodable, Identifiable {
    var id: Double { return dt }
    
    let dt: Double
    let temp: Temp
    let weather: [WeatherDescription]
}

struct WeatherForecast: Decodable {
    let current: Weather
    let daily: [DailyWeather]
}
