//
//  City.swift
//  Kursinis iOS
//
//  Created by Aurintas on 2020-05-07.
//  Copyright © 2020 VU. All rights reserved.
//

struct City: Decodable, Identifiable {
    var id: String { return name }
    
    let country: String
    let name: String
    let lon: Double
    let lat: Double
    
    init(country: String, name: String, lon: Double, lat: Double) {
        self.country = country
        self.name = name
        self.lon = lon
        self.lat = lat
    }
    
    enum CodingKeys: String, CodingKey {
        case country = "name"
        case name = "capital"
        case latlng
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let latlng = try values.decode([Double].self, forKey: .latlng)
        lat = latlng[0]
        lon = latlng[1]
        country = try values.decode(String.self, forKey: .country)
        name = try values.decode(String.self, forKey: .name)
    }
}
