import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import countries from '../../constants/countries.json';
import CountryItem from './CountryItem/CountryItem';

const Home = () => {
  return (
    <View style={styles.container}>
      <FlatList
        data={countries}
        renderItem={({
          item: {
            name,
            capital,
            latlng: [lat, lon],
          },
        }) => <CountryItem country={name} capital={capital} lat={lat} lon={lon} />}
        keyExtractor={item => item.country_code}
        contentInsetAdjustmentBehavior="always"
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
