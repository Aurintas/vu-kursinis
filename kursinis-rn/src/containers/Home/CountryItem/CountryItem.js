import React, { memo } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import RouteNames from '../../../RouteNames';

const CountryItem = ({ country, capital, lat, lon }) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate(RouteNames.WeatherForecast, { city: capital || country, lat, lon })
      }
      style={styles.container}
    >
      <Text style={styles.text}>
        {country}
        {capital ? `, ${capital}` : ''}
      </Text>
    </TouchableOpacity>
  );
};

export default memo(CountryItem);

const styles = StyleSheet.create({
  container: {
    marginLeft: 16,
    paddingVertical: 16,
    borderColor: 'lightgrey',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  text: {
    fontSize: 16,
  },
});
