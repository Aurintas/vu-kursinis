import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { format, fromUnixTime } from 'date-fns';

const DailyForecastItem = ({
  day: {
    dt,
    temp,
    weather: [{ icon }],
  },
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.weekDay}>{format(fromUnixTime(dt), 'EEEE')}</Text>

      <Text style={styles.temp}>
        <Text style={styles.dayTemp}>{Math.round(temp.day)} </Text> / {Math.round(temp.night)}
      </Text>

      <Image
        source={{ uri: `http://openweathermap.org/img/wn/${icon}@2x.png` }}
        style={styles.image}
        resizeMode="contain"
      />
    </View>
  );
};

export default DailyForecastItem;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    paddingVertical: 16,
    paddingHorizontal: 16,
    alignItems: 'center',
    borderColor: 'lightgrey',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  weekDay: {
    fontSize: 16,
    flex: 1,
  },
  image: {
    width: 20,
    height: 20,
    marginLeft: 16,
  },
  temp: {
    fontSize: 16,
  },
  dayTemp: {
    fontWeight: 'bold',
  },
});
