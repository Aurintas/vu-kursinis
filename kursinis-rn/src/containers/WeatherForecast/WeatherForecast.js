import React, { useEffect, useLayoutEffect } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Image, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCurrentWeather } from '../../state/weather/WeatherActions';
import { getCurrentWeather } from '../../state/weather/WeatherSelectors';
import { useNavigation } from '@react-navigation/native';
import DailyForecastItem from './DailyForecastItem/DailyForecastItem';

const WeatherForecast = ({
  route: {
    params: { city, lat, lon },
  },
}) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({ headerTitle: city });
  }, [navigation, city]);

  const currentWeather = useSelector(getCurrentWeather);

  useEffect(() => {
    dispatch(fetchCurrentWeather(lat, lon));
  }, []);

  if (!currentWeather) {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={StyleSheet.absoluteFill} size="large" />
      </View>
    );
  }

  const {
    current: {
      weather: [weather],
      temp,
    },
    daily,
  } = currentWeather;
  const { icon } = weather;

  return (
    <ScrollView
      style={styles.container}
      contentInsetAdjustmentBehavior="always"
      contentContainerStyle={styles.content}
    >
      <Image
        source={{ uri: `http://openweathermap.org/img/wn/${icon}@2x.png` }}
        style={styles.image}
        resizeMode="contain"
      />

      <View style={styles.tempContainer}>
        <Text style={styles.temp}>{Math.round(temp)}</Text>
        <Text style={styles.tempUnits}>°C</Text>
      </View>

      {daily.map(day => (
        <DailyForecastItem key={day.dt} day={day} />
      ))}
    </ScrollView>
  );
};

export default WeatherForecast;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    paddingBottom: 16,
  },
  image: {
    width: 100,
    height: 100,
    alignSelf: 'center',
  },
  tempContainer: {
    flexDirection: 'row',
    marginVertical: 24,
    alignSelf: 'center',
  },
  temp: {
    fontSize: 32,
    fontWeight: 'bold',
    alignItems: 'flex-start',
  },
  tempUnits: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 4,
    marginTop: 3,
  },
});
