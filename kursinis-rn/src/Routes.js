import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RouteNames from './RouteNames';
import Home from './containers/Home/Home';
import WeatherForecast from './containers/WeatherForecast/WeatherForecast';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={RouteNames.Home}
          component={Home}
          options={{ headerTitle: 'Orai Sostinėse' }}
        />
        <Stack.Screen name={RouteNames.WeatherForecast} component={WeatherForecast} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
