import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Routes from './Routes';
import configureStore from './state/configureStore';
import { StatusBar } from 'react-native';
// import { getTimeSinceStartup } from 'react-native-startup-time';

const { store, persistor } = configureStore();

class App extends React.Component {
  constructor(props) {
    super(props);

    // getTimeSinceStartup().then(time => {
    //   alert(`Time since startup: ${time} ms`);
    // });
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <Routes />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
