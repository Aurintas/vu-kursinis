import axiosModule from 'axios';
import config from '../../config';

const axios = axiosModule.create({
  baseURL: 'https://api.openweathermap.org/data/2.5',
});

axios.interceptors.request.use(request => {
  request.params = request.params || {};
  request.params.appid = config.OPEN_WEATHER_API_KEY;
  return request;
});

class OpenWeatherApi {
  getCurrentWeather = cityName => axios.get(`weather?q=${cityName}&units=metric&lang=lt`);
  getWeather = (lat, lon) => axios.get(`onecall?lat=${lat}&lon=${lon}&units=metric&lang=lt`);
}

export default new OpenWeatherApi();
