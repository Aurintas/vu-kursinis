import WeatherConstants from './WeatherConstants';

export const fetchCurrentWeather = (lat, lon) => ({
  type: WeatherConstants.FETCH_CURRENT_WEATHER,
  payload: { lat, lon },
});

export const setCurrentWeather = weather => ({
  type: WeatherConstants.SET_CURRENT_WEATHER,
  payload: { weather },
});
