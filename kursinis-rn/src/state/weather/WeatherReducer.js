import WeatherConstants from './WeatherConstants';

const INITIAL_STATE = {
  current: null,
  forecast: {},
};

export default function weatherReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case WeatherConstants.SET_CURRENT_WEATHER: {
      return {
        ...state,
        current: action.payload.weather,
      };
    }
    default:
      return state;
  }
}
