import { call, put, takeLatest } from 'redux-saga/effects';

import WeatherConstants from './WeatherConstants';
import OpenWeatherApi from '../../apis/OpenWeatherApi';
import { setCurrentWeather } from './WeatherActions';

function* handleFetchCurrentWeather({ payload: { lat, lon } }) {
  try {
    yield put(setCurrentWeather(null));

    const { data } = yield call(OpenWeatherApi.getWeather, lat, lon);

    yield put(setCurrentWeather(data));
  } catch (e) {
    console.error(e);
  }
}

export default function* weatherSaga() {
  yield takeLatest(WeatherConstants.FETCH_CURRENT_WEATHER, handleFetchCurrentWeather);
}
