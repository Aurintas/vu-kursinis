import { combineReducers } from 'redux';

import weather from './weather/WeatherReducer';

const combinedReducer = combineReducers({
  weather,
});

const rootReducer = (state, action) => combinedReducer(state, action);

export { rootReducer };
