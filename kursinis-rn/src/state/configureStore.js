import AsyncStorage from '@react-native-community/async-storage';
import { persistReducer, persistStore } from 'redux-persist';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './reducers';
import { rootSaga } from './sagas';
import createSagaMiddleware from 'redux-saga';

const persistorConfig = {
  key: '@kursinis-rn:state',
  storage: AsyncStorage,
  whitelist: ['weather'],
};

export default (initialState = {}) => {
  const persistedReducer = persistReducer(persistorConfig, rootReducer);
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(persistedReducer, initialState, applyMiddleware(sagaMiddleware));
  const persistor = persistStore(store);
  sagaMiddleware.run(rootSaga);

  return { store, persistor };
};
