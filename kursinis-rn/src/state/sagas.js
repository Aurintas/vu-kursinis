import { all, fork } from 'redux-saga/effects';

import weatherSaga from './weather/WeatherSaga';

export function* rootSaga() {
  yield all([fork(weatherSaga)]);
}
